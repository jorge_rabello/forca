# Forca

A simple app to study clojure lang.

# To build a jar

At the root directory do this

```
$ lein uberjar
```

Your .jar file will be placed at target/uberjar directory, so to execute you can run 
```
$ java -jar target/uberjar/forca-0.1.0-SNAPSHOT-standalone.jar
```

# To run it

If you clone this and just want run, execute this, at the root directory

```
$ lein run
```

## Installation

First you need to install the clojure lang its too easy just see this:

https://clojure.org/guides/getting_started


You will need to isntall Leiningen, this will help us to create projects and use the repl console tool.

https://leiningen.org/

## Steps to install Leiningen on Linux

1- Get the lein script:  
https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein

and save it to a directory on your home.

Eg.: /home/user/bin or ~/bin - here is important that you have full permissions on the selected directory.

2 - Place the directory to your $PATH variable I use my ~/.profile file to do this

see lines 17 and 18:

```
 17 LEIN_HOME=/home/user/bin
 18 PATH=$PATH:$JAVA_HOME/bin:$LEIN_HOME
``` 

3 - Give the following permissions to the lein script

``` 
$ chmod a+x ~/bin/lein
``` 

4 - Finally you can run the lein script

``` 
$ lein
``` 

## Using lein repl
``` 
$ lein repl

nREPL server started on port 37627 on host 127.0.0.1 - nrepl://127.0.0.1:37627
REPL-y 0.4.3, nREPL 0.6.0
Clojure 1.10.0
OpenJDK 64-Bit Server VM 1.8.0_212-8u212-b03-0ubuntu1.16.04.1-b03
    Docs: (doc function-name-here)
          (find-doc "part-of-name-here")
  Source: (source function-name-here)
 Javadoc: (javadoc java-object-or-class-here)
    Exit: Control+D or (exit) or (quit)
 Results: Stored in vars *1, *2, *3, an exception in *e

user=> (+ 5 7)
12
user=>
user=> exit
Bye for now!
``` 

## Creating a project using lein

``` 
$ lein new app your_project_name 
$ cd you_project_name_directory
``` 
### ATENTION: 

Type the name of your project using lower case ok ?! ;D

## To test this
``` 
$ cd your_project_directory
$ lein repl
forca.core=> total-de-vidas
6
forca.core=> 
``` 
